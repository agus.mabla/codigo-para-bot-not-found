# Error 404 - Copa Robótica 2019

## Descripción

Este repositorio contiene el la ultima versión recopílada del codigo para **Bot Not Found**, nuestro robót para la edición nacional de First Global en 2019.

### Integrantes
- Tomás Raspa
- Candela Heredia 
- Francisco García
- Leandro Horane
- Agustín Blanco

### Profesores
- Silvina Rodriguez
- Pablo Fiscella
- Mariano Santamaria